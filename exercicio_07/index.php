<?php
/*

####### OBSERVAÇÕES SOBRE DOCUMENTAÇÃO #######

A documentação da API foi feita em YAML, com foco em execução no Swagger.
O arquivo encontra-se no mesmo diretório que a API, com o nome "definition.yaml".

Para verificar ela através de uma interface de usuário, basta carregar no Swagger, ou online em:
    https://editor.swagger.io/

Se necessário realizar testes a partir da interface Swagger, a definição "servers/url" precisar estar de acordo com o caminho da API.
Por padrão está configurado em localhost, porta 8080.

O arquivo test.php também possui testes simples via CURL, para facilitar o entendimento

*/

// -- Header apenas para fazer testes com requisições entre dois HOSTs diferentes, sem precisar se preocupar com maiores questões de segurança
header('Access-Control-Allow-Origin: *');

// -- Função básica para encerrar execução em caso de detecção de falta de param
function invalidParam($objApi) {
    $err = $objApi->apiMessage(false, "Parâmetros inválidos");
    echo json_encode($err);
    die();
}

require_once("api.class.php");
$objApi = new ApiController();

// Obtém o verbo HTTP, e o conteúdo do REQUEST
$method = $_SERVER["REQUEST_METHOD"];
$input = json_decode(file_get_contents("php://input"));

// -- Se existir o parâmetro objUser, atribui na variável $objUser
$objUser = (isset($input->objUser)) ? $input->objUser : null;

// -- Checa o verbo HTTP recebido e invoca a funcionalidade de acordo com o mesmo
switch ($method) {
    case "GET":
        //echo "método GET invocado\r\n<br />";
        $paramEmail = (isset($_GET['email'])) ? $_GET['email'] : null;
        $result = $objApi->getUser($paramEmail);
        echo json_encode($result);
        break;
    case "PUT":
        //echo "método PUT invocado\r\n<br />";
        $paramEmail = (isset($input->email)) ? $input->email : null;
        if ($objUser == null) {
            invalidParam($objApi);
        }
        $result = $objApi->putUser($input->email, $objUser);
        echo json_encode($result);
        break;
    case "POST":
        //echo "método POST invocado\r\n<br />";
        if ($objUser == null) {
            invalidParam($objApi);
        }
        $result = $objApi->postUser($objUser);
        echo json_encode($result);
        break;
    case "DELETE":
        //echo "método DELETE invocado\r\n<br />";
        $paramEmail = (isset($_GET['email'])) ? $_GET['email'] : $input->email;
        $result = $objApi->deleteUser($paramEmail);
        echo json_encode($result);
        break;
    default:
        header("HTTP/1.1 405 Method Not Allowed");
        header("Allow: GET, PUT, POST, DELETE");
        break;
}