<?php

/*
    Classe controladora da API para a funções básicas de controle de registro
    Implementa os métodos GET, POST, PUT e DELETE, que são invocados de acordo com os filtros realizados
*/
class ApiController {
    private $_file = "registros";

    // -- Obtém uma linha (registro) específica no arquivo de acordo com o email informado
    // -- Caso não seja informado nenhum email para pesquisa, retorna TODOS os registros
    function getUser($email = null) {
        $arrRetorno = array();
        if (file_exists($this->_file.".txt")) {
            $readOnly = fopen($this->_file.".txt", 'r');
            while (($line = fgets($readOnly)) !== false) {
                $line = str_replace("\r\n", "", $line);
                $aux = explode(";", $line);
                // -- Caso seja solicitada uma linha específica (busca por email)
                if ($email !== null) {
                    // Índice 2: e-mail
                    if ($aux[2] == $email) {
                        return array(
                            "nome" => $aux[0],
                            "sobrenome" => $aux[1],
                            "email" => $aux[2]
                        );
                    }
                }
                // -- Se solicitou todas as linhas, concatena os resultados no $arrRetorno
                else {
                    $arrRetorno[] = array(
                        "nome" => $aux[0],
                        "sobrenome" => $aux[1],
                        "email" => $aux[2]
                    );
                }
            }
            fclose($readOnly);
        }
        return $arrRetorno;
    }

    // -- Modifica uma linha (registro) específica do arquivo
    function putUser($email, $objUser) {
        $affectedRows = $this->changeFileLine($email, "UPDATE", $this->objToLine($objUser));
        return $this->apiMessage(true, "Processo de atualização finalizado. " . $affectedRows . " linha(s) afetada(s).");
    }

    // -- Remove uma linha (registro) específica do arquivo
    function deleteUser($email) {
        $affectedRows = $this->changeFileLine($email, "DELETE");
        return $this->apiMessage(true, "Processo de remoção finalizado. " . $affectedRows . " linha(s) afetada(s).");
    }

    // -- Insere uma linha (registro) no arquivo com os dados informados
    function postUser($objUser) {
        file_put_contents($this->_file.".txt", $this->objToLine($objUser)."\r\n", FILE_APPEND);
        return $this->apiMessage(true, "Processo de inserção finalizado.");
    }
    
    function apiMessage($success, $message = "") {
        return array(
            "success" => $success,
            "message" => $message);
    }

    // -- Método "Helper" para converter objUser em string formatada para ser gravada
    function objToLine($objUser) {
        return $objUser->nome . ";" . $objUser->sobrenome . ";" . $objUser->email;
    }

    /* Modifica ou remove uma linha específica do arquivo, de acordo com o email informado
        $email -> chave de busca
        $action
            UPDATE: Percorre as linhas até achar o email correspondente e troca a linha toda por $lineValue
            DELETE: Percorre as linhas até achar o email correspondente e remove essa linha
    */
    function changeFileLine($email, $action = "UPDATE", $lineValue = null) {
        if($action != "UPDATE" && $action != "DELETE") {
            return null; // Ação inválida
        }

        $affectedRows = 0;
        if (file_exists($this->_file.".txt")) {
            $hasChanged = false;
            $readOnly = fopen($this->_file.".txt", 'r');
            $tmpFile = fopen($this->_file.".tmp", 'w');
            while (($line = fgets($readOnly)) !== false) {
                $aux = explode(";", trim($line));
                // Índice 2: e-mail
                if ($aux[2] == $email) {
                    if ($action == "UPDATE") {
                        $hasChanged = true;
                        $affectedRows++;
                        // -- Altera toda a linha
                        $line = $lineValue."\r\n";
                    }
                    elseif($action == "DELETE") {
                        $hasChanged = true;
                        $affectedRows++;
                        // -- Continua para próxima iteração do WHILE. Não incluindo a linha a ser removida
                        continue;
                    }
                }
                // -- Salva as linhas lidas no arquivo tmp
                fputs($tmpFile, $line);
            }
            fclose($readOnly);
            fclose($tmpFile);
            
            // -- Transforma o arquivo temporário em definitivo e remove o tmp permanentemente
            // -- Só realiza o processo caso alguma alteração seja detectada
            if ($hasChanged) {
                rename($this->_file.".tmp", $this->_file.".txt");
            } else {
                unlink($this->_file.".tmp");
            }
        }
        return $affectedRows;
    }
}
