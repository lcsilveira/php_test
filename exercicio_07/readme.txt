

####### OBSERVAÇÕES SOBRE DOCUMENTAÇÃO #######

A documentação da API foi feita em YAML, com foco em execução no Swagger.
O arquivo encontra-se no mesmo diretório que a API, com o nome "definition.yaml".

Para verificar ela através de uma interface de usuário, basta carregar no Swagger, ou online em:
    https://editor.swagger.io/

Se necessário realizar testes a partir da interface Swagger, a definição "servers/url" precisar estar de acordo com o caminho da API.
Por padrão está configurado em localhost, porta 8080.

O arquivo test.php também possui testes simples via CURL, para facilitar o entendimento
