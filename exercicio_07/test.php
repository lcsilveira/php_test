<?php
// Este arquivo contém testes rápidos de invocação da API

// -- Param "email", utilizado para filtros ou seleção de registro
$email = "lucas@test.com";

// -- Por padrão está configurado em localhost, pora 8080
$url = "http://localhost:8080/php_test/exercicio_07/index.php?email=" . $email;
$header = array('Content-Type: application/json', 'Accept: application/json');

// -- Array com os dados de usuário a Inserir ou Editar (de acordo com o verbo HTTP)
$arrUser = array(
    "nome" => "Lucas",
    "sobrenome" => "Silveira",
    "email" => $email
);
$data = array(
    "email" => $email,
    "objUser" => $arrUser);

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data)); // Dados transferidos em JSON
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT'); // Modificar para mudar o método invocado

$result = curl_exec($ch);
$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

// -- Converte retorno para exibição em tela
header('Content-Type: application/json');
echo $result;

curl_close($ch);
