<?php

// -- Gera 0 ou 1, que no PHP é convertido implicitamente para FALSE e TRUE, respectivamente
function foiMordido() {
    return rand(0,1);
}

// -- Coloquei o retorno da função em uma variável apenas para validar o retorno da mesma
$mordeu = foiMordido();

// -- Código que escreve exatamente o retorno da função
//var_dump($mordeu); echo "<br>";

// -- Como trata-se de uma comparação simples, o IF ternário deixa o código mais curto, sem atrapalhar a compreensão 
echo "Joãozinho " . (!$mordeu ? "NÃO" : "") . "  mordeu o seu dedo!";
