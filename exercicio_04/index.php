
<!doctype html>
<html lang="en">
<head>
    <title>Exercício 04</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
    <!--
        Formulário com os campos propostos pelo exercício
        A estrutura foi montada de modo a ficar com visual simples, porém amigável, através das classes do Bootstrap
    -->
    <div class="container">
    <div class="row">
        <div class="col-md-3"> &nbsp; </div>
        <div class="col-md-6">
            <form action="save.php" method="POST" onSubmit="return validaForm()">
                <div class="form-group">
                    <label for="nome">Nome</label>
                    <input type="text" maxlength="100" class="form-control" name="nome" id="nome" placeholder="Primeiro nome" required>
                </div>
                <div class="form-group">
                    <label for="sobrenome">Sobrenome</label>
                    <input type="text" maxlength="100" class="form-control" name="sobrenome" id="sobrenome" placeholder="Sobrenome" required>
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" maxlength="100" class="form-control" name="email" id="email" placeholder="Endereço de email" required>
                </div>
                <div class="form-group">
                    <label for="telefone">Telefone</label>
                    <input type="text" maxlength="15" class="form-control phone" name="telefone" id="telefone" placeholder="Telefone de contato" required>
                    <p class="text-danger" id="avisoTelefone" style="display:none">Informe um telefone válido!</p>
                </div>
                <div class="form-group">
                    <label for="login">Login</label>
                    <input type="text" maxlength="50" class="form-control" name="login" id="login" placeholder="Login de acesso" required>
                </div>
                <div class="form-group">
                    <label for="senha">Senha</label>
                    <input type="password" maxlength="20" class="form-control" name="senha" id="senha" required>
                </div>
                <button type="submit" class="btn btn-primary">Salvar</button>
            </form>
        </div>
    </div>
    </div>
    
    <script src="js/jquery-3.2.1.slim.min.js"></script>
    <script src="js/phone-mask.js"></script>
    <script src="js/bootstrap.min.js"></script>

<script>
    // -- Componente open source baseado em jQuery para máscara automática de telefone com 8 e 9 dígitos
    // -- Obtido no repositório: https://github.com/masimao/maskbrphone
    // -- Incluído no arquivo phone-mask.js
    $('.phone').maskbrphone({  
        useDdd           : true, // Define se o usuário deve digitar o DDD  
        useDddParenthesis: true, // Informa se o DDD deve estar entre parênteses  
        dddSeparator     : ' ',  // Separador entre o DDD e o número do telefone  
        numberSeparator  : '-'   // Caracter que separa o prefixo e o sufixo do telefone  
    });

    function validaForm() {
        $('#avisoTelefone').hide();
        $objTelefone = $('#telefone');
        sizeOfPhone = $objTelefone.val().length;
        if(sizeOfPhone != 14 && sizeOfPhone != 15) {
            $objTelefone.focus();
            $('#avisoTelefone').show();
            return false;
        }
        return true;
    }
</script>

</body>
</html>