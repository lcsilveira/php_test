<?php
if(empty($_POST)) {
	header("location: index.php");
	die();
}
?>
<!doctype html>
<html lang="en">
<head>
    <title>Exercício 04</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<body padding>

<?php

// -- Função básica apenas para auxiliar na montagem do link de retorno ao formulário
function backLink() {
    return "<br /><button class='btn btn-primary' onclick=\"location.href='index.php'\">Voltar ao formulário</button>";
}

$arrInputs = array(
    "nome" => $_POST["nome"],
    "sobrenome" => $_POST["sobrenome"],
    "email" => $_POST["email"],
    "telefone" => $_POST["telefone"],
    "login" => $_POST["login"],
    "senha" => $_POST["senha"],
);

foreach($arrInputs as $key => $val) {
    /* Como trata-se de uma segunda validação (todas essas validação já são feitas antes de enviar o formulário),
          não há grandes preocupações com a estética da mensagem. Apenas com a segurança em não gravar dados incoerentes
    */
    if($val == "") {
        die("<p class='text-center'>O campo '" . $key . "' é obrigatório!" . backLink() . "</p>");
    }

    if($key == "telefone") {
        // -- Verifica se o telefone é válido (com DDD + Máscara deve possuir 14 ou 15 caracteres (dependendo do 9º dígito))
        if(strlen($val) != 14 && strlen($val) != 15) {
            die("<p class='text-center'>Informe um telefone válido!" . backLink() . "</p>");
        }
        // -- Remove os caracteres de máscara e espaço do campo telefone. Ou seja, salva apenas números
        $arrInputs[$key] = preg_replace("/[^0-9]/", "", $val);
    }

    // -- Aplica hash MD5 na senha
    if($key == "senha") {
        $arrInputs[$key] = md5($val);
    }
}

// -- Verifica se o arquivo já existe
if (file_exists("registros.txt")) {
    $handle = fopen("registros.txt", "r");
    if ($handle) {
        // -- Percorre as linhas do arquivo verificando se email/login já existem
        while (($line = fgets($handle)) !== false) {
            // -- Caso já exista um registro para esse email/login, o processo é encerrado
            $aux = explode(";", $line);
            // Índice 2: e-mail e 4: login
            if ($aux[2] == $arrInputs["email"] || $aux[4] == $arrInputs["login"]) {
                die("<p class='text-center'>Email e/ou Login já cadastrados!" . backLink() . "</p>");
            }
        }
        fclose($handle);
    }
}

// -- Separa os dados do array por ";" e salva uma quebra de linha
file_put_contents("registros.txt", implode(";",$arrInputs)."\r\n", FILE_APPEND);

echo "<p class='text-center'>Dados salvos com sucesso!" . backLink() . "</p>";

?>

</body>
</html>