<?php

// -- Construção do array do país. Para o exemplo, foram utilizados 5 valores fixos.
$location = array(
    array('pais' => 'Brasil', 'capital' => 'Brasília'),
    array('pais' => 'EUA', 'capital' => 'Washington'),
    array('pais' => 'Espanha', 'capital' => 'Madrid'),
    array('pais' => 'Irlanda', 'capital' => 'Dublim'),
    array('pais' => 'Portugal', 'capital' => 'Lisboa')
);

// -- $capitalOrder: array utilizado para ordenação na função array_multisort().
// -- Ele será montado com as capitais como valores unidimensionais
$capitalOrder = array();
foreach ($location as $key => $row)
{
    $capitalOrder[$key] = $row['capital'];
}

// -- Ordena $location com base na lógica de ordenação alfabética (SORT_ASC) de $capitalOrder
array_multisort($capitalOrder, SORT_ASC, $location);

// -- Escreve array na tela, de maneira estruturada
echo "<pre>" . print_r($location, true) . "</pre>";
