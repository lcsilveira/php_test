<?php
    // -- Inclusão do arquivo com a classe proposta pelo exercício
    require_once("select-field.class.php");

    $arrPerfis = array(
        1 => "Administrador",
        2 => "Consulta",
        3 => "Lorem Ipsum"
    );
    // -- Instancia a classe no $objSelectPerfil
    $objSelectPerfil = new selectField("perfil", $arrPerfis, "form-control", " required");

    //print_r($objSelectPerfil->getValues());
?>

<!doctype html>
<html lang="en">
<head>
    <title>Exercício 06</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<body>

    <div class="container">
        <div class="row">
            <div class="col-md-3"> &nbsp; </div>
            <div class="col-md-6">
                <form action="" method="POST">
                    <div class="form-group">
                        <label for="login">Login</label>
                        <input type="text" maxlength="50" class="form-control" name="login" id="login" placeholder="Login de acesso" required>
                    </div>
                    <div class="form-group">
                        <label for="perfil">Perfil</label>
                        <?php
                            // -- Cria o HTML do select e escreve na estrutura da página
                            echo $objSelectPerfil->printHtml();
                        ?>
                    </div>
                    <div class="form-group">
                        <label for="senha">Senha</label>
                        <input type="password" maxlength="20" class="form-control" name="senha" id="senha" required>
                    </div>
                    <button type="submit" class="btn btn-primary">Salvar</button>
                </form>
            </div>
        </div>
    </div>

    <script src="js/jquery-3.2.1.slim.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

</body>
</html>