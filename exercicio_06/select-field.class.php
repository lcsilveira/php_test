<?php

// -- Classe para montar um Select Field através
class selectField {
    /*  $inputName -> será utilizado para as propriedade "name" e "id" (com prefixo "sel") do HTML
        $arrValuesParam -> array com opções que serão escritas no HTML do select. Ex: array(VALUE => DISPLAYTEXT)
        $cssClasses -> string com as classes de css do elemento
        $html -> string para complementar a tag <select>, por exemplo " required onclick='func()'"
    */ 
    private $_arrValues = array();
    private $_inputName = "";
    private $_cssClasses = "";
    private $_html = "";

    function __construct($inputName = "", $arrValuesParam = array(), $cssClasses = "", $html = "") {
        $this->_inputName = $inputName;
        $this->_arrValues = $arrValuesParam;
        $this->_cssClasses = $cssClasses;
        $this->_html = $html;
    }

    // -- Adiciona classe css
    function addCssClass($class) {
        $this->_cssClasses = $this->_cssClasses . " " . $class;
    }

    // -- Remove classe css
    function removeCssClass($class) {
        // -- Explode a string de classes, para garantir que vai remover apenas o termo pesquisado
        $arrAux = explode(" ", $this->_cssClasses);
        // -- Limpa as classes atuais
        $this->_cssClasses = "";
        // -- Percorre todas as atuais classes
        foreach($arrAux as $value) {
            // -- Caso a classe analisada seja diferente da classe que será removida
            if($value != $class) {
                $this->addCssClass($value);
            }
        }
    }

    // -- "Reescreve" o array de valores que serão utilizados para montar o select field
    function setValues($arrValuesParam = array()) {
        $this->_arrValues = $arrValuesParam;
    }

    // -- Obtém o array de valores que serão utilizados para montar o select field
    function getValues() {
        return $this->_arrValues;
    }

    // -- Monta e retorna o HTML do select field
    function printHtml() {
        $strOutput = "<select name='" . $this->_inputName . "' 
                            id='sel_" . $this->_inputName . "' 
                            class='" . $this->_cssClasses . "' " . $this->_html . ">";
        foreach($this->_arrValues as $key => $displayText) {
            $key = addslashes($key);
            $displayText = addslashes($displayText);
            $strOutput .= "<option value='" . $key . "'>" . $displayText . "</option>";
        }
        $strOutput .= "</select>";
        return $strOutput;
    }
}
