<?php

// -- Array de arquivos proposto pelo exercício
$arrFiles = array(
    'music.mp4',
    'video.mov',
    'imagem.jpeg'
);

// -- Array que será utilizado para armazenar apenas as extensões
$arrExtensions = array();
foreach($arrFiles as $filename) {
    $aux = explode(".", $filename);
    $extension = "." . end($aux);
    $arrExtensions[] = $extension;
}

// -- Ordena em ordem alfabética
sort($arrExtensions, SORT_STRING);

// -- Escreve uma lista ordenada na tela (usando HTML básico)
echo "<ol>";
foreach($arrExtensions as $extension) {
    echo "<li> $extension </li>";
}
echo "</ol>";