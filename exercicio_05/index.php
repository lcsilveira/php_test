<?php

// -- Função que roda recursivamente lendo o XML e escrevendo seus nós em linhas do CSV
function processaXML($xmlFile, $csvFile) {
    foreach ($xmlFile->children() as $node) {
        // -- Se o nó atual não possui mais níveis
        if (count($node->children()) == 0) {
            // -- Armazena as informações da linha atual em um array, junto com o nome do nó
            $arrAux = array($node->getName(), $node);
            // -- Salva o array em uma linha do CSV
            fputcsv($csvFile, $arrAux, ",", "\"");
        } else {
            // -- Se possuir mais níveis, a função é chamada recursivamente
            processaXML($node, $csvFile);
        }
    }
}

// -- Arquivo XML a ser convertido
$xmlName = "conteudo.xml";
$csvName = "output.csv";
if (file_exists($xmlName)) {
    // -- Carrega o XML para a variável $xmlFile
    echo $xmlFile = simplexml_load_file($xmlName);
    // -- Abre o arquivo CSV em modo de escrita. Se não existir, será criado. Se existir, terá o conteúdo zerado
    $csvFile = fopen($csvName, 'w+');

    // -- Chama principal função para manipulação dos nós do XML
    processaXML($xmlFile, $csvFile);
    fclose($csvFile);

    echo "O processamento do arquivo XML foi finalizado! Arquivo de saída: " . $csvName;
}

